2020.10.01
  - Fixes: ILP64, CMake output padding

2020.10.00
  - Fixes: CMake version
  - Add `make check`

2020.09.00
  - Initial release.
    - Supports LAPACK >= 3.2.1.
    - Includes routines through LAPACK 3.7.0.
    - Makefile and CMake build options
